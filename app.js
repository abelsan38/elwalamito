document.addEventListener('DOMContentLoaded', () => {
    apps();
})
function apps() {
    eventListener();
    scrollnav();
}
//header estatico

// window.onscroll = function () {
//     const header = document.querySelector('header');
//     if (window.scrollY > 100) {
//         if (!header.classList.contains('fixed-header')) {
//             header.style.transition = 'top 0.3s ease';
//             header.classList.add('fixed-header');
//         }
//     } else {
//         if (header.classList.contains('fixed-header')) {
//             header.style.transition = 'top 0.3s ease';
//             header.classList.remove('fixed-header');
//             // Ajusta la posición al valor deseado
//             header.style.top = '0';
//         }
//     }
// };


//navegacion fluida

function scrollnav() {
    const enlaces = document.querySelectorAll('.nav a');
    enlaces.forEach(enlace => {
        enlace.addEventListener('click', (e) => {
            e.preventDefault();
            const seseccionScroll = e.target.attributes.href.value;
            const seccion = document.querySelector(seseccionScroll);
            seccion.scrollIntoView({ behavior: "smooth" });
        })
    })
}
//menu hamburguesa

const navegacion = document.querySelector('.nav');
const catalogo = document.querySelector('#ca');
const contacto = document.querySelector('#con');
const ini = document.querySelector('#in');
const ub = document.querySelector('#ib');


catalogo.addEventListener('click', () => {
    navegacion.classList.remove('mostrar')
})
contacto.addEventListener('click', () => {
    navegacion.classList.remove('mostrar')
})
function eventListener() {
    const body = document.querySelector('#body');
    const menu = document.querySelector('.menu');
    menu.addEventListener('click', navresponsive);
    body.addEventListener('click', fuera);
    ini.addEventListener('click', inicio);
    ub.addEventListener('click', ubicacion);
}
function ubicacion() {
    const navegacion = document.querySelector('.nav');
    navegacion.classList.remove('mostrar')

}
function inicio() {
    const navegacion = document.querySelector('.nav');
    navegacion.classList.remove('mostrar')

}
function nosotross() {
    const navegacion = document.querySelector('.nav');
    navegacion.classList.remove('mostrar')

}
function fuera() {
    const navegacion = document.querySelector('.nav');
    navegacion.classList.remove('mostrar');
}
function navresponsive() {
    const navegacion = document.querySelector('.nav');
    navegacion.classList.toggle('mostrar');
}
function revealElementsOnScroll() {
    const elements = document.querySelectorAll('.element-to-reveal');
    elements.forEach(element => {
        const elementTop = element.getBoundingClientRect().top;
        const windowBottom = window.innerHeight;

        if (elementTop < windowBottom) {
            element.classList.add('reveal');
        }
    });
}

// Ejecuta la función cuando se carga la página y cuando se desplaza.
window.addEventListener('load', revealElementsOnScroll);
window.addEventListener('scroll', revealElementsOnScroll);
/**--------------------------------------------------------------- */
/**OPERACIONES DE LOS DIAS Y PRECIOS(HORAS) */
// let res = document.getElementById('hora');
// function calcularPesos() {
//     let hora = document.getElementById('grua').value;

//     resultado = hora * 650;

//     res.innerHTML = `<p>El precio por ${hora} horas: $${resultado}</p>`;
// }
// let calcular = document.getElementById('calcular');
// calcular.addEventListener('click', () => {
//     calcularPesos();
// });
// let limpiar = document.getElementById('limpiar');
// limpiar.addEventListener('click', () => {
//     document.getElementById('grua').value = "";
//     res.innerHTML = "";
// });
// //grua 17 toneladas
// let res2 = document.getElementById('hora2');
// function calcularPesos2() {
//     let hora = document.getElementById('grua17').value;

//     resultados = hora * 800;

//     res2.innerHTML = `<p>El precio por ${hora} horas: $${resultados}</p>`;
// }
// let calcular2 = document.getElementById('calcular2');
// calcular2.addEventListener('click', () => {
//     calcularPesos2();
// });
// let limpiar2 = document.getElementById('limpiar2');
// limpiar2.addEventListener('click', () => {
//     document.getElementById('grua17').value = "";
//     res2.innerHTML = "";

// });
// //grua 20 toneladas
// let res3 = document.getElementById('hora3');
// function calcularPesos3() {
//     let hora = document.getElementById('grua20').value;

//     resultados = hora * 1000;

//     res3.innerHTML = `<p>El precio por ${hora} horas: $${resultados}</p>`;
// }
// let calcular3 = document.getElementById('calcular3');
// calcular3.addEventListener('click', () => {
//     calcularPesos3();
// });
// let limpiar3 = document.getElementById('limpiar3');
// limpiar3.addEventListener('click', () => {
//     document.getElementById('grua20').value = "";
//     res3.innerHTML = "";
// });
// //grua 40 toneladas
// let res4 = document.getElementById('hora4');
// function calcularPesos3() {
//     let hora = document.getElementById('grua40').value;

//     resultados = hora * 1500;

//     res4.innerHTML = `<p>El precio por ${hora} horas: $${resultados}</p>`;
// }
// let calcular4 = document.getElementById('calcular4');
// calcular4.addEventListener('click', () => {
//     calcularPesos3();
// });
// let limpiar4 = document.getElementById('limpiar4');
// limpiar4.addEventListener('click', () => {
//     document.getElementById('grua40').value = "";
//     res4.innerHTML = "";
// });
// //grua toneladas
// let res5 = document.getElementById('hora5');
// function calcularPesos4() {
//     let hora = document.getElementById('grua1').value;

//     resultados = hora * 800;

//     res5.innerHTML = `<p>El precio por ${hora} horas: $${resultados}</p>`;
// }
// let calcular5 = document.getElementById('calcular5');
// calcular5.addEventListener('click', () => {
//     calcularPesos4();
// });
// let limpiar5 = document.getElementById('limpiar5');
// limpiar5.addEventListener('click', () => {
//     document.getElementById('grua1').value = "";
//     res5.innerHTML = "";
// });
// //pipa toneladas
// let res6 = document.getElementById('hora6');
// function calcularPesos5() {
//     let hora = document.getElementById('pipa').value;

//     resultados = hora * 1500;

//     res6.innerHTML = `<p>El precio por ${hora} horas: $${resultados}</p>`;
// }
// let calcular6 = document.getElementById('calcular6');
// calcular6.addEventListener('click', () => {
//     calcularPesos5();
// });
// let limpiar6 = document.getElementById('limpiar6');
// limpiar6.addEventListener('click', () => {
//     document.getElementById('pipa').value = "";
//     res6.innerHTML = "";
// });
// //Montacargas de 8 toneladas
// let res7 = document.getElementById('dias1');
// function calcularPesos6() {
//     let hora = document.getElementById('Montacargas8t').value;

//     resultados = hora * 3500;

//     res7.innerHTML = `<p>El precio por ${hora} dias: $${resultados}</p>`;
// }
// let calcular7 = document.getElementById('calcular7');
// calcular7.addEventListener('click', () => {
//     calcularPesos6();
// });
// let limpiar7 = document.getElementById('limpiar7');
// limpiar7.addEventListener('click', () => {
//     document.getElementById('Montacargas8t').value = "";
//     res7.innerHTML = "";
// });
// //retroexcavadora
// let res8 = document.getElementById('hora7');
// function calcularPesos7() {
//     let hora = document.getElementById('retroexcavadora').value;

//     resultados = hora * 800;

//     res8.innerHTML = `<p>El precio por ${hora} dias: $${resultados}</p>`;
// }
// let calcular8 = document.getElementById('calcular8');
// calcular8.addEventListener('click', () => {
//     calcularPesos7();
// });
// let limpiar8 = document.getElementById('limpiar8');
// limpiar8.addEventListener('click', () => {
//     document.getElementById('retroexcavadora').value = "";
//     res8.innerHTML = "";
// });
// //d6
// let res9 = document.getElementById('hora8');
// function calcularPesos8() {
//     let hora = document.getElementById('retroexcavadora').value;

//     resultados = hora * 2000;

//     res9.innerHTML = `<p>El precio por ${hora} dias: $${resultados}</p>`;
// }
// let calcular9 = document.getElementById('calcular9');
// calcular9.addEventListener('click', () => {
//     calcularPesos8();
// });
// let limpiar9 = document.getElementById('limpiar9');
// limpiar9.addEventListener('click', () => {
//     document.getElementById('d6').value = "";
//     res9.innerHTML = "";
// });
// //retroexcavadoramar
// let res10 = document.getElementById('hora9');
// function calcularPesos9() {
//     let hora = document.getElementById('retroexcavadoramar').value;

//     resultados = hora * 1200;

//     res10.innerHTML = `<p>El precio por ${hora} dias: $${resultados}</p>`;
// }
// let calcular10 = document.getElementById('calcular10');
// calcular10.addEventListener('click', () => {
//     calcularPesos9();
// });
// let limpiar10 = document.getElementById('limpiar10');
// limpiar10.addEventListener('click', () => {
//     document.getElementById('retroexcavadoramar').value = "";
//     res10.innerHTML = "";
// });
// //cimbra
// let res11 = document.getElementById('M2');
// function calcularPesos10() {
//     let hora = document.getElementById('cimbra').value;

//     resultados = hora * 120;

//     res11.innerHTML = `<p>El precio por ${hora} M2: $${resultados}</p>`;
// }
// let calcular11 = document.getElementById('calcular11');
// calcular11.addEventListener('click', () => {
//     calcularPesos10();
// });
// let limpiar11 = document.getElementById('limpiar11');
// limpiar11.addEventListener('click', () => {
//     document.getElementById('cimbra').value = "";
//     res11.innerHTML = "";
// });
// //andamios
// let res12 = document.getElementById('quincena');
// function calcularPesos12() {
//     let hora = document.getElementById('andamios').value;

//     resultados = hora * 300;

//     res12.innerHTML = `<p>El precio por ${hora} M2: $${resultados}</p>`;
// }
// let calcular12 = document.getElementById('calcular12');
// calcular12.addEventListener('click', () => {
//     calcularPesos12();
// });
// let limpiar12 = document.getElementById('limpiar12');
// limpiar12.addEventListener('click', () => {
//     document.getElementById('andamios').value = "";
//     res12.innerHTML = "";
// });
// //demoledora
// let res13 = document.getElementById('dias2');
// function calcularPesos13() {
//     let hora = document.getElementById('demoledora').value;

//     resultados = hora * 400;

//     res13.innerHTML = `<p>El precio por ${hora} dias: $${resultados}</p>`;
// }
// let calcular13 = document.getElementById('calcular13');
// calcular13.addEventListener('click', () => {
//     calcularPesos13();
// });
// let limpiar13 = document.getElementById('limpiar13');
// limpiar13.addEventListener('click', () => {
//     document.getElementById('demoledora').value = "";
//     res13.innerHTML = "";
// });
// //makita
// let res14 = document.getElementById('dias3');
// function calcularPesos14() {
//     let hora = document.getElementById('makita').value;

//     resultados = hora * 500;

//     res14.innerHTML = `<p>El precio por ${hora} dias: $${resultados}</p>`;
// }
// let calcular14 = document.getElementById('calcular14');
// calcular14.addEventListener('click', () => {
//     calcularPesos14();
// });
// let limpiar14 = document.getElementById('limpiar14');
// limpiar14.addEventListener('click', () => {
//     document.getElementById('makita').value = "";
//     res14.innerHTML = "";
// });